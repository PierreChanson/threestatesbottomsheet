package com.example.pierrechanson.threestatesbottomsheet;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class InvoiceListAdapter extends RecyclerView.Adapter<InvoiceListAdapter.ViewHolder> {



    private List<String> items;

    public InvoiceListAdapter(List<String> items) {
        this.items = items;
    }

    @Override
    public InvoiceListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_bill, null);

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        // - get data from your items at this position
        // - replace the contents of the view with that items
        final String item = items.get(position);

        viewHolder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(),"clicked on: "+ position, Toast.LENGTH_SHORT).show();
            }
        });
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtViewTitle;
        public TextView separator;
        public View itemLayout;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            itemLayout = itemLayoutView;
            txtViewTitle = (TextView) itemLayoutView.findViewById(R.id.list_item_bill_title);
            separator = (TextView) itemLayoutView.findViewById(R.id.list_item_bill_separator);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
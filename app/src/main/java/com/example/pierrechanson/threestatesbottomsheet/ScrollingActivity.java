package com.example.pierrechanson.threestatesbottomsheet;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

public class ScrollingActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private ThreeStatesBottomSheetBehavior sheetBehavior;
    private FloatingActionButton fab;
    private TextView header;
    private ImageView cat;
    private Button bike;
    private View bottomSheet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        List<String> items = Arrays.asList("BuenA", "Córdoba", "La Plata", "fafasfasfas",
                "fafasfafasfahh", "Buenos Aires", "Córdoba", "La Plata", "fafasfasfas",
                "fafasfafasfahh", "La Plata", "La Plata", "La Plata", "La Plata", "La Plata",
                "La Plata", "La Plata", "La Plata", "La Plata", "La Plata", "La Plata", "La Plata");
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        bottomSheet = findViewById(R.id.bottom_sheet_layout);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        header = (TextView) findViewById(R.id.header);
        cat = (ImageView) findViewById(R.id.app_top_content);
        bike = (Button) findViewById(R.id.bike_marker);

        final CoordinatorLayout.LayoutParams p = (CoordinatorLayout.LayoutParams) fab.getLayoutParams();
        sheetBehavior = ThreeStatesBottomSheetBehavior.from(bottomSheet);
        sheetBehavior.setBottomSheetCallback(new ThreeStatesBottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                Log.d("bottom sheet", "onStateChanged: " + newState);
                // React to state change
                if (newState == ThreeStatesBottomSheetBehavior.STATE_HIDDEN) {
                    Log.d("bottom sheet", "HIDE");
                } else if (newState == ThreeStatesBottomSheetBehavior.STATE_COLLAPSED) {
                    Log.d("bottom sheet", "COLLAPSED");
                } else if (newState == ThreeStatesBottomSheetBehavior.STATE_SETTLING) {
                    Log.d("bottom sheet", "SETTLING");
                } else if (newState == ThreeStatesBottomSheetBehavior.STATE_EXPANDED) {
                    Log.d("bottom sheet", "EXTANDED");
                } else if (newState == ThreeStatesBottomSheetBehavior.STATE_MIDDLE) {
                    Log.d("bottom sheet", "MIDDLE");
                }

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                Log.d("bottom sheet", "slideOffset: " + slideOffset);
                // React to dragging events
                // React to state change
            }
        });
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView = (RecyclerView)findViewById(R.id.list_bills);
        recyclerView.setLayoutManager(layoutManager);
        InvoiceListAdapter mAdapter = new InvoiceListAdapter(items);
        recyclerView.setAdapter(mAdapter);

        setCallbacks();
    }

    private void setCallbacks(){
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int state = sheetBehavior.getMajorState();
                if (state == 4) {
                    sheetBehavior.setState(ThreeStatesBottomSheetBehavior.STATE_MIDDLE);
                } else if (state == 6) {
                    sheetBehavior.setState(ThreeStatesBottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });
        cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sheetBehavior.setState(ThreeStatesBottomSheetBehavior.STATE_HIDDEN);
            }
        });
        bike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sheetBehavior.setState(ThreeStatesBottomSheetBehavior.STATE_COLLAPSED);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
